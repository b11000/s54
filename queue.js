let collection = [];
// Write the queue functions below.
// Functions to be implemented
	// printQueue()
	// enqueue(item)
	// dequeue()
	// front()
	// size()
	// isEmpty()

	// [1] Print queue elements.
	//The printed value is an empty array. RESULT = [] (3 ms)
	function print(){
		return collection
	}
	// [2] Enqueue a new element.
	// The value has been enqueued. RESULT = ['John'] (1 ms)
	// [3] Enqueue another element.
	// The value has been enqueued. RESULT = ['John', 'Jane'] (1 ms)
	// [5] Enqueue another element.
	// The value has been enqueued. RESULT = ['Jane', 'Bob']
	// [6] Enqueue another element.
	// The value has been enqueued. RESULT = ['Jane', 'Bob', 'Cherry']
	function enqueue(i){

	collection[collection.length] = i

	return collection
	}
	// [4] Dequeue the first element.
	// The value has been dequeued. RESULT = ['Jane']
	function dequeue(){
		let newCollection = []

		for (let i = 0; i < collection.length; i++){
			if(i!=0){
				newCollection[i-1]=collection[i]
			}
		}
		collection=newCollection
		return collection
	}
	// [7] Get first element.
	// The first value has been retrieved. RESULT = 'Jane' (1 ms)
	function front(){
		return collection[0]
	}
	// [8] Get queue size.
	// The size of the queue has been retrieved. RESULT = 3 (1 ms)
	function size(){
		return collection.length
	}
	// [9] Check if queue is not empty.
	// The result has been retrieved. RESULT = false
	function isEmpty() {
	return collection.length ? false : true;
	}

module.exports = {
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty
};